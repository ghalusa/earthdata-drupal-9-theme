<?php

/**
 * @file
 * Functions to support theming in the SASS Starterkit subtheme.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;
// use Drupal\Core\Url;
use Drupal\Component\Utility\Html;

/**
 * Implements hook_form_system_theme_settings_alter() for settings form.
 *
 * Replace Barrio setting options with subtheme ones.
 */
function earthdata_form_system_theme_settings_alter(&$form, FormStateInterface $form_state) {
  $form['components']['navbar']['bootstrap_barrio_navbar_top_background']['#options'] = array(
    'bg-primary' => t('Primary'),
    'bg-secondary' => t('Secondary'),
    'bg-light' => t('Light'),
    'bg-dark' => t('Dark'),
    'bg-white' => t('White'),
    'bg-transparent' => t('Transparent'),
  );
  $form['components']['navbar']['bootstrap_barrio_navbar_background']['#options'] = array(
    'bg-primary' => t('Primary'),
    'bg-secondary' => t('Secondary'),
    'bg-light' => t('Light'),
    'bg-dark' => t('Dark'),
    'bg-white' => t('White'),
    'bg-transparent' => t('Transparent'),
  );
}

/**
 * Content Types
 *
 * @return array
 */
function _earthdata_theme_content_types() {
  return [
    'article',
    'page',
    'pathfinder_page',
    'news_page',
    'event',
    'backgrounder',
    'worldview_image_of_the_week',
    'data_chat',
    'data_user_profile',
    'tutorial'
  ];
}


/**
 * Node Titles
 *
 * @return array
 */
function _earthdata_theme_node_titles() {
  return [
  ];
}

/**
 * Field As Block module labels
 *
 * @return array
 */
function _earthdata_theme_field_as_block_labels() {
  return [
    'Term Name',
    'Term Description',
    'Title',
    'Summary',
    'Term Title',
    'Program Menu'
  ];
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function earthdata_theme_suggestions_field_alter(array &$suggestions, array $variables) {
  // Get the URL alias ($url_alias).
  $current_path = \Drupal::service('path.current')->getPath();
  $current_route = \Drupal::routeMatch()->getRouteName();
  $url_alias = \Drupal::service('path_alias.manager')->getAliasByPath($current_path);

  // Target the content field (field__node__field_content).
  // If the 'style-guide' string is found in the URL alias, generate a theme suggestion for the style guide.
  // The theme file name should end up being: field--node--style-guide.html.twig
  // Note how dashes are used in the theme file, but underscores are used in the .theme file.

  // Style Guide
  if (in_array('field__node__field_content', $suggestions) && stristr($url_alias, 'style-guide')) {
    $suggestions[] = 'field__node__style_guide';
  }
}

/**
 * Implements hook_preprocess_region().
 */
function earthdata_preprocess_region(&$variables) {

  if ($variables['region'] !== 'hero') {
    return;
  }
  $data = $taxonomy_term = $node = FALSE;
  // Allowed content types.
  $variables['content_types'] = _earthdata_theme_content_types();
  $variables['content_type'] = FALSE;
  $variables['vocabulary'] = FALSE;
  $variables['term_child'] = FALSE;
  $taxonomy_term = \Drupal::routeMatch()->getParameter('taxonomy_term');
  $node = \Drupal::routeMatch()->getParameter('node');
  $data = !empty($taxonomy_term) ? $taxonomy_term : $node;

  // dd($data);

  // Hero images for entities using field_hero_image.
  if (!empty($data)) {

    // Get the content type if it's a node.
    $variables['content_type'] = $node ? $data->getType() : FALSE;

    if ($data->hasField('field_hero_image')) {
      // Allowed node titles.
      $variables['node_titles'] = _earthdata_theme_node_titles();
      // Field As Block module labels
      $variables['labels'] = _earthdata_theme_field_as_block_labels();
      // Get the media's target id.
      $mid = $data->get('field_hero_image')->target_id;
      // Set a bollean variable indicating if this is a child term or not.
      if ($taxonomy_term) {
        // Set the vocabulary (e.g., gcmd, daac).
        $variables['vocabulary'] = $data->vid->target_id;
        $variables['term_child'] = $data->parent->target_id !== '0' ? TRUE : FALSE;
      }
      // Set the hero_image_url variable.
      if (!empty($mid)) {
        $fid = Media::load($mid)->field_media_image[0]->getValue()['target_id'];
        $file = File::load($fid);
        $variables['hero_image_url'] = $file->createFileUrl();
        // Set the title of the taxonomy term or node.
        if ($taxonomy_term) {
          $variables['title'] = $data->hasField('field_title') ? $data->get('field_title')->value : $data->getName();
        } else {
          $variables['title'] = $data->getTitle();
        }
      }
    }

   // // Pathfinder pages
   // if (method_exists($data, 'getType') && in_array($data->getType(), $variables['content_types'])) {
   //   // Allowed node titles.
   //   $variables['node_titles'] = _earthdata_theme_node_titles();
   //    // Field As Block module labels
   //    $variables['labels'] = _earthdata_theme_field_as_block_labels();
   //    // Set the hero_image_url variable.
   //    switch ($data->getType()) {
   //      case 'pathfinder_page':
   //        $variables['hero_image_url'] = '/sites/default/files/pathfinder/pathfinder-title-background.jpg';
   //        break;
   //      default:
   //        $variables['hero_image_url'] = '';
   // //        break;
   //    }
   //    // Set the title of the node.
   //    $variables['title'] = $data->getTitle();
   //  }

    // dd($variables);
  }

}

/**
 * Implements hook_preprocess_block().
 */
function earthdata_preprocess_block(&$variables) {

  $data = $taxonomy_term = $node = FALSE;
  $variables['term_child'] = FALSE;
  $variables['vocabulary'] = FALSE;
  // Allowed content types.
  $variables['content_types'] = _earthdata_theme_content_types();
  $taxonomy_term = \Drupal::routeMatch()->getParameter('taxonomy_term');
  $node = \Drupal::routeMatch()->getParameter('node');
  $data = !empty($taxonomy_term) ? $taxonomy_term : $node;

  // Hero images for Topic and DAAC landing pages.
  if (!empty($data)) {

    // dd($data);

    // Make sure the field exists.
    if ($data->hasField('field_hero_image') || (method_exists($data, 'getType') && in_array($data->getType(), $variables['content_types']))) {
      // Allowed node titles.
      $variables['node_titles'] = _earthdata_theme_node_titles();
      // Field As Block module labels
      $variables['labels'] = _earthdata_theme_field_as_block_labels();
      // Set the vocabulary (e.g., gcmd, daac).
      if (!empty($data->vid->target_id)) {
        $variables['vocabulary'] = $data->vid->target_id;
      }
      // Set the Get Data URI.
      if ($data->hasField('field_find_data') && !empty($data->field_find_data->getValue())) {
        $variables['get_data_uri'] = $data->field_find_data->getValue()[0]['uri'];
      }

      // If the topic is a parent topic (parent id is zero - 0),
      // set the term_slug, term_name, and term_description variables.
      if ($taxonomy_term) {
        switch ($variables['configuration']['label']) {
          case 'Term Name':
          case 'Term Description':
          case 'Term Icon':
          case 'Find Data Button':
            // Only return the term slug for the top level parent topics.
            $variables['term_slug'] = ($data->parent->target_id === '0') ? Html::getClass($data->getName()) : FALSE;
            // Return the term_name and term_description variables for all topics.
            $variables['term_name'] = $data->hasField('field_title') ? $data->get('field_title')->value : $data->getName();
            $variables['term_description'] = $data->hasField('field_description') ? $data->get('field_description')->value : $data->getDescription();
            // Supress the 'Definition unavailable' term description.
            $variables['term_description'] = $variables['term_description'] !== 'Definition unavailable' ? $variables['term_description'] : '';
            break;
        }
      }
      // Set a bollean variable indicating if this is a child term or not.
      if ($taxonomy_term && ($data->parent->target_id !== '0')) {
        $variables['term_child'] = TRUE;
      }
      // If this is a node...
      if ($node) {
        switch ($variables['configuration']['label']) {
          case 'Title':
          case 'Summary':
              $variables['node_type'] = $data->getType();
              $variables['node_slug'] = Html::getClass($data->getTitle());
              $variables['node_title'] = $data->getTitle();
              $variables['node_summary'] = $data->hasField('field_summary') ? $data->get('field_summary')->value : '';
              $variables['node_top_nav'] = $data->hasField('field_top_nav') ? $data->get('field_top_nav') : '';

              // Summary background
              if($data->hasField('field_summary_background')){
                $mid = $data->get('field_summary_background')->target_id;
                if (!empty($mid)) {
                  $fid = Media::load($mid)->field_media_image[0]->getValue()['target_id'];
                  $file = File::load($fid);
                  $variables['summary_background_url'] = $file->createFileUrl();
                }
              }
              $variables['node_summary_background'] = $data->hasField('field_summary_background') ? $data->get('field_summary_background')->value : '';

              // $variables['node_top_nav'] = $data->hasField('field_top_nav') ? $data->get('field_top_nav')->target_id : '';
              // dump($variables['node_top_nav']);

              break;
        }
      }

    }
        if ($data->hasField('field_hero_image')) {
          // Allowed node titles.
          $variables['node_titles'] = _earthdata_theme_node_titles();
          // Field As Block module labels
          $variables['labels'] = _earthdata_theme_field_as_block_labels();
          // Get the media's target id.
          $mid = $data->get('field_hero_image')->target_id;
          // Set a bollean variable indicating if this is a child term or not.
          if ($taxonomy_term) {
            // Set the vocabulary (e.g., gcmd, daac).
            $variables['vocabulary'] = $data->vid->target_id;
            $variables['term_child'] = $data->parent->target_id !== '0' ? TRUE : FALSE;
          }
          // Set the hero_image_url variable.
          if (!empty($mid)) {
            $fid = Media::load($mid)->field_media_image[0]->getValue()['target_id'];
            $file = File::load($fid);
            $variables['hero_image_url'] = $file->createFileUrl();
            // Set the title of the taxonomy term or node.
            if ($taxonomy_term) {
              $variables['title'] = $data->hasField('field_title') ? $data->get('field_title')->value : $data->getName();
            } else {
              $variables['title'] = $data->getTitle();
            }
          }
        }

  }

}

/**
 * Implements hook_preprocess_views_view().
 */
function earthdata_preprocess_views_view(&$variables) {

  // Content types.
  $variables['content_types'] = _earthdata_theme_content_types();
  $node = \Drupal::routeMatch()->getParameter('node');
  $data = !empty($taxonomy_term) ? $taxonomy_term : $node;

  //dd($data);

  if (!empty($data)) {

    $variables['node_type'] = $data->getType();

  }

}

/**
 * Implements hook_preprocess_field().
 */
function earthdata_preprocess_field(&$variables) {

  // Modify the node_author field.
  if ($variables['element']['#field_name'] === 'node_author') {
    // Get the username from the node_author field.
    $user_name = $variables['items'][0]['content'][0]['#markup'];
    // Load the 'group' user via username.
    $user = user_load_by_name($user_name);
    // Get the value of the field_full_name field.
    $full_name = $user->get('field_full_name')->value;
    // If the field_full_name value is not empty, override the default username.
    if (!empty($full_name)) {
      $variables['items'][0]['content'][0]['#markup'] = $full_name;
    }
    // Get the value of the field_byline field.
    $byline = $user->get('field_byline')->value;
    // If the field_byline value is not empty, add it to the author output.
    if (!empty($byline)) {
      $variables['items'][0]['content'][0]['#markup'] = $full_name . ' - ' . $byline;
    }
  }

  // ESDS-481: Remove line breaks from GCMD-based taxonomy term descriptions.
  if ($variables['entity_type'] === 'taxonomy_term' && $variables['element']['#field_name'] === 'description') {
    $new_line = array("\r\n", "\n", "\r");
    $variables['items'][0]['content']['#text'] = str_replace($new_line, ' ', $variables['items'][0]['content']['#text']);
  }

}

/**
* Implements earthdata_suggestions_page_alter().
*/
function earthdata_theme_suggestions_page_alter(array &$suggestions, array $variables) {
  if ($node = \Drupal::routeMatch()->getParameter('node')) {
    $suggestions[] = 'page__' . $node->bundle();
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter() for form templates.
 * @param array $suggestions
 * @param array $variables
 */
function earthdata_theme_suggestions_block_alter(array &$suggestions, array $variables) {
  // Block suggestions for custom block bundles.
  if (isset($variables['elements']['content']['#block_content'])) {
    array_splice($suggestions, 1, 0, 'block__bundle__' . $variables['elements']['content']['#block_content']->bundle());
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter() for form templates.
 * @param array $suggestions
 * @param array $variables
 */
function earthdata_theme_suggestions_views_view_list_alter(array &$suggestions, array $variables) {
  //dump($variables);
  $view = $variables['view'];
  $id = $view->storage->id();
  if ($id === 'child_terms') {
    $suggestions[] = sprintf('views_view_list__%s', $id);
  }
}

/**
 * Implements template_preprocess_html().
 * @param array $variables
 */
function earthdata_preprocess_html(&$variables) {
  $variables['host'] = \Drupal::request()->getHost();
}

/**
 * Implements hook_preprocess_page().
 * @param array $variables
 */
function earthdata_preprocess_page(&$variables) {

  $host = \Drupal::request()->getHost();

  // Include analytics JavaScript in the <head>, only on production.
  // Google Tag Manager, Crazyegg, and Universal Federated Analytics from dap.digitalgov.gov.
  if ($host === 'www.earthdata.nasa.gov') {
    $variables['#attached']['library'][] = 'earthdata/js-header';
  }

  // Include non-analytics JavaScript in the footer.
  $variables['#attached']['library'][] = 'earthdata/js-footer';

  // Get the URL alias ($url_alias).
  $current_path = \Drupal::service('path.current')->getPath();
  $current_route = \Drupal::routeMatch()->getRouteName();
  $url_alias = \Drupal::service('path_alias.manager')->getAliasByPath($current_path);

  // Attach specific CSS and JS libraries to targeted pages.
  switch ($url_alias) {
    case '/eosdis/daacs':
      $variables['#attached']['library'][] = 'earthdata/interactive-map';
      break;
    case '/homepage':
      $variables['#attached']['library'][] = 'earthdata/home-interactive';
      break;
    default:
      // Do nothing
      break;
  }

}

/**
 * Implements hook_preprocess_paragraph().
 * @param array $variables
 */
function earthdata_preprocess_paragraph(&$variables) {

  // Pathfinder pages: group first tier and second tier tabs together.
  if (is_object($variables['paragraph']->bp_header) &&
    ($variables['paragraph']->bp_header->value === 'First Tier')) {

    // Get the node.
    $node = \Drupal::routeMatch()->getParameter('node');

    // Get the Paragraphs.
    if (!empty($node)) {

      $paragraphs = $node->get('field_bootstrap_component');

      // Loop through paragraphs to return render arrays.
      if (!empty($paragraphs->referencedEntities())) {

        foreach ($paragraphs->referencedEntities() as $key => $p) {

          // $variables['tabs']['id'][$key] = $p->id->value;
          // $variables['tabs']['bp_header'][$key] = $p->bp_header->view('full');
          // $variables['tabs']['bp_tab_section'][$key] = $p->bp_tab_section->view('full');

          $variables['tabs']['id'][$key] = $p->id->value;

          if(method_exists($p->bp_header, 'view')) {
            $variables['tabs']['bp_header'][$key] = $p->bp_header->view('full');
          }

          if(method_exists($p->bp_tab_section, 'view')) {
            $variables['tabs']['bp_tab_section'][$key] = $p->bp_tab_section->view('full');
          }

        }

      }

    }

  }

}

/**
 * Implements hook_preprocess_aggregator_feed().
 * @param array $variables
 */
function earthdata_preprocess_aggregator_feed(&$variables) {
  $number_of_items = 3;
  $variables['content']['items'] = array_slice($variables['content']['items'], 0, 2 + $number_of_items);
}
