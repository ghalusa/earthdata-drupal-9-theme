# NASA Earthdata Drupal 9 Theme

## Contents of this File

* Introduction
* Requirements
* Maintainers/Support


### Introduction

NASA's Earthdata Drupal 9 theme, a SASS-based subtheme of [Bootstrap Barrio](https://www.drupal.org/project/bootstrap_barrio) (Bootstrap 4).


### Requirements

* N/A


### Maintainers/Support

* Goran Halusa: [goran.halusa@ssaihq.com](mailto:goran.halusa@ssaihq.com)
* [Science Systems and Applications](https://ssaihq.com/) (SSAI)