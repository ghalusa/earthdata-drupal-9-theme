/**
 * @file
 * Global utilities.
 *
 */

(function($) {

  // Search button / input toggle functionality.
  var searchSpanImage = $('.search-span img'),
      searchButton = $('.search-span .search-icon-desktop'),
      searchInput = $('.search-container-desktop #searchright')
  // On click:
  $(document).on('click', function(e) {
    // If the search-icon is clicked.
    if (e.target.className === 'search-icon-desktop') {
      searchInput.animate({width: 'toggle', duration: '100'})
      searchSpanImage.attr('src', (_, attr) => attr == '/themes/custom/earthdata_drupal_9_theme/images/icon-x.png' ? '/themes/custom/earthdata_drupal_9_theme/images/search.png' : '/themes/custom/earthdata_drupal_9_theme/images/icon-x.png')
      e.stopPropagation()
      e.preventDefault()
    } else {
      if (e.target.className !== 'search expandright') {
        // Clicking anything else.
        searchInput.fadeOut('fast')
        searchSpanImage.attr('src', '/themes/custom/earthdata_drupal_9_theme/images/search.png')
      }
    }
  })

  // Horizontal Nav Widget: Toggle the rotation of the arrow image.
  var topNavMenuButtons = document.getElementsByClassName('topnav-button');

  if (typeof topNavMenuButtons !== 'undefined') {
    // Add event listeners for all menus.
    for (var i = topNavMenuButtons.length - 1; i >= 0; i--) {
      // Show
      topNavMenuButtons[i].addEventListener('show.bs.dropdown', function () {
        $(this).parent().find('.arrow-box img').toggleClass('arrow-down');
      });
      // Hidden
      topNavMenuButtons[i].addEventListener('hidden.bs.dropdown', function () {
        $(this).parent().find('.arrow-box img').removeClass('arrow-down');
      });
    }
  }

  $('.arrow-box').on('click', function(e){
    e.stopPropagation();
    let thisDropdownId = $(this).parent().parent().find('button').attr('id');
    let thisDropdown = document.getElementById(thisDropdownId);
    new bootstrap.Dropdown(thisDropdown).toggle();
  });

  // Mobile nav toggle.
  var mobileNav = $('#mobile-nav');

  $('.btn-navbar').on('click', function(){
    mobileNav.toggleClass('d-none');
    $('.breadcrumb-item').toggle();
  });

  // Hide menu if clicked outside.
  $('#main-wrapper').on('click', function(e){
    if (!mobileNav.is(e.target) && mobileNav.has(e.target).length === 0 && !mobileNav.hasClass('d-none')){
      $('#mobile-nav').toggleClass('d-none');
      $('.breadcrumb-item').show();
    }
  });

  // Hide menu on window resize.
  $( window ).resize(function() {
    $('#mobile-nav').addClass('d-none');
    $('.breadcrumb-item').show();
  });

  // Submit the search form on icon click (mobile).
  $('.search-icon-mobile').on('click', function(e){
    $('#search-form-mobile').submit();
  });

})(jQuery);

document.addEventListener("DOMContentLoaded", () => {
  const trigger = document.querySelector(`ul.nav a[href="${window.location.hash}"]`)
  if (trigger) {
    const tab = new bootstrap.Tab(trigger)
    tab.show()
  }
})
