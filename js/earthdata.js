/**
 * @file
 * Earthdata Global utilities.
 *
 */
(function($, Drupal) {

  'use strict';

  Drupal.behaviors.earthdata = {
    attach: function(context, settings) {

      if(Drupal.AjaxCommands){
        Drupal.AjaxCommands.prototype.viewsScrollTop = null;
      }

    }
  };

})(jQuery, Drupal);
