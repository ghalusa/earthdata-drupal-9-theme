// uses https://leafletjs.com/examples/crs-simple/crs-simple.html

// initial map creation
var map = L.map('map', {
  crs: L.CRS.Simple, // CRS.Simple take the form of [y, x] instead of [x, y]
    zoomControl: false,
    maxZoom: 0
});
var bounds = [[0,0], [595,966]];
var image = L.imageOverlay('/themes/custom/earthdata_drupal_9_theme/js/map/images/map.png', bounds).addTo(map); // custom map image
map.fitBounds(bounds);

// set variable for custom marker icon
var markerIcon = L.icon({
  iconUrl: '/themes/custom/earthdata_drupal_9_theme/js/map/images/marker-custom.png',
  iconSize: [ 16, 16 ]
});

// individual locations
var po = L.marker([ 262, 104 ], // set the location of the marker
    {icon: markerIcon}) // include custom marker
  .bindTooltip("<div class='upper'><span class='name'>PHYSICAL OCEANOGRAPHY DAAC (PO.DAAC)</span><p>Pasadena, CA</p></div><p>Gravity, ocean winds, sea surface temperature, ocean surface topography, sea surface salinity, circulation</p>", // tooltip popup text
    { autoPan: false }) // prevents map from getting bumped
  .addTo(map) // add marker to map (this line is different than the clustered markers)
  .on('click', function(e) {
    window.location = '/eosdis/daacs/podaac'; // marker link as an on-click
  });

var asf = L.marker([ 104, 143 ],
    {icon: markerIcon})
  .bindTooltip("<div class='upper'><span class='name'>ALASKA SATELLITE FACILITY (ASF) DAAC</span><p>Fairbanks, AK</p></div><p>Synthetic Aperture Radar (SAR)</p>",
    { autoPan: false })
  .addTo(map)
  .on('click', function(e) {
    window.location = '/eosdis/daacs/asf';
  });

var nsidc = L.marker([ 345, 340 ],
    {icon: markerIcon})
  .bindTooltip("<div class='upper'><span class='name'>NATIONAL SNOW AND ICE DATA CENTER (NSIDC) DAAC</span><p>Boulder, CO</p></div><p>Snow, sea ice, glaciers, ice sheets, ice shelves, frozen ground, soil moisture, cryosphere, climate interactions</p>",
    { autoPan: false })
  .addTo(map)
  .on('click', function(e) {
    window.location = '/eosdis/daacs/nsidc';
  });

var lp = L.marker([ 412, 478 ],
    {icon: markerIcon})
  .bindTooltip("<div class='upper'><span class='name'>LAND PROCESSES DAAC (LP DAAC)</span><p>Sioux Falls, SD</p></div><p>Land cover, surface reflectance, radiance, temperature, topography, vegetation indices</p>",
    { autoPan: false })
  .addTo(map)
  .on('click', function(e) {
    window.location = '/eosdis/daacs/lpdaac';
  });

var ghrc = L.marker([ 232, 651 ],
    {icon: markerIcon})
  .bindTooltip("<div class='upper'><span class='name'>GLOBAL HYDROMETEOROLOGY RESOURCE CENTER (GHRC) DAAC</span><p>Huntsville, AL</p></div><p>Lightning, tropical cyclones, and storm-induced hazards</p>",
    { autoPan: false })
  .addTo(map)
  .on('click', function(e) {
    window.location = '/eosdis/daacs/ghrc';
  });

var ornl = L.marker([ 264, 689 ],
    {icon: markerIcon})
  .bindTooltip("<div class='upper'><span class='name'>OAK RIDGE NATIONAL LABORATORY (ORNL) DAAC</span><p>Oak Ridge, TN</p></div><p>Snow, sea ice, glaciers, ice sheets, ice shelves, frozen ground, soil moisture, cryosphere, climate interactions</p>",
    { autoPan: false })
  .addTo(map)
  .on('click', function(e) {
    window.location = '/eosdis/daacs/ornl';
  });

var asdc = L.marker([ 298, 815 ],
    {icon: markerIcon})
  .bindTooltip("<div class='upper'><span class='name'>ATMOSPHERIC SCIENCE DATA CENTER (ASDC)</span><p>Hampton, VA</p></div><p>Radiation budget, clouds, aerosols, and tropospheric composition</p>",
    { autoPan: false })
  .addTo(map)
  .on('click', function(e) {
    window.location = '/eosdis/daacs/asdc';
  });

var sedac = L.marker([ 399, 837 ],
    {icon: markerIcon})
  .bindTooltip("<div class='upper'><span class='name'>SOCIOECONOMIC DATA AND APPLICATIONS CENTER (SEDAC)</span><p>Palisades, NY</p></div><p>Human interactions in the environment</p>",
    { autoPan: false })
  .addTo(map)
  .on('click', function(e) {
    window.location = '/eosdis/daacs/sedac';
  });


// clustered locations
var markers = L.markerClusterGroup({
  // don't animate when spiderfying
  animate: false
});

var cddis = L.marker(new L.latLng([ 348, 800 ]), // set the location of the marker
    {icon: markerIcon}) // include custom marker
  .bindTooltip("<div class='upper'><span class='name'>CRUSTAL DYNAMICS DATA INFORMATION SYSTEM (CDDIS)</span><p>Greenbelt, MD</p></div><p>Space geodesy</p>", // tooltip popup text
    { autoPan: false }) // prevents map from getting bumped
  .on('click', function(e) {
    window.location = '/eosdis/daacs/cddis'; // marker link as an on-click
  });
markers.addLayer(cddis); // add marker to the layer that will get added at the end

var gesdisc = L.marker(new L.latLng([ 348, 800 ]),
    {icon: markerIcon})
  .bindTooltip("<div class='upper'><span class='name'>GODDARD EARTH SCIENCES DATA AND INFORMATION SERVICES CENTER (GES DISC)</span><p>Greenbelt, MD</p></div><p>Atmospheric composition, atmospheric dynamics, global precipitation, solar irradiance</p>",
    { autoPan: false })
  .on('click', function(e) {
    window.location = '/eosdis/daacs/gesdisc';
  });
markers.addLayer(gesdisc);

var laads = L.marker(new L.latLng([ 348, 800 ]),
    {icon: markerIcon})
  .bindTooltip("<div class='upper'><span class='name'>LEVEL 1 AND ATMOSPHERE ARCHIVE AND DISTRIBUTION SYSTEM (LAADS) DAAC</span><p>Greenbelt, MD</p></div><p>MODIS Level 1 and atmosphere products</p>",
    { autoPan: false })
  .on('click', function(e) {
    window.location = '/eosdis/daacs/laads';
  });
markers.addLayer(laads);

var ob = L.marker(new L.latLng([ 348, 800 ]),
    {icon: markerIcon})
  .bindTooltip("<div class='upper'><span class='name'>OCEAN BIOLOGY DAAC (OB.DAAC)</span><p>Greenbelt, MD</p></div><p>Ocean color, sea surface temperature, sea surface salinity</p>",
    { autoPan: false })
  .on('click', function(e) {
    window.location = '/eosdis/daacs/obdaac';
  });
markers.addLayer(ob);

// adds clustered markers to the map
map.addLayer(markers);

// spiderfy the cluster
map.eachLayer(function(layer) {
  if (layer._leaflet_id === 43) {
    layer.spiderfy();
  }
});
