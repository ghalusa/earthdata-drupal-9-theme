/**
 * @file
 * Megamenu utilities.
 *
 */

(function($) {

  // Move the child fly-outs to the third column.
  var trigger = $('.dropdown-submenu'),
      targetBlock = $('.tb-megamenu-block[data-block="earthdata_branding"]');

  targetBlock.hide();

  $('.tb-megamenu-item').hover(function(e) {
    targetBlock.hide();
  });

  trigger.hover(function(e) {

    var thisElement = $(this),
        thisSource = thisElement.find('.tb-megamenu-submenu').find('.tb-megamenu-subnav'),
        thisHref = thisElement.find('a.dropdown-toggle');

    if (thisSource.length === 0) {
      thisSource = thisElement.find('.tb-megamenu-block').find('.field--name-body');
    }

    if (thisSource.length) {

      var clonedSource = thisSource.clone();
      thisSource.hide();
      thisSource.closest('.mega-dropdown-menu').hide();

      targetBlock.empty();
      targetBlock.append(clonedSource);
      targetBlock.show();
      targetBlock.find('div:first-child').show();
      targetBlock.find('ul').show();

      thisHref.on('click tap', function(e) {
        window.location.href = thisHref.attr('href');
      });
    }

  });

})(jQuery);
