window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
/* Our GA */
gtag('config', 'UA-219533781-1');
/* Their GA */
gtag('config', 'UA-62340125-1');
