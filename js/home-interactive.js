// JavaScript Document
(function($) {
	$('#nav > div').hover(
		function () {
			var $this = $(this);
			// Increase the z-index value.
			$this.addClass('z-index-increase');
			$this.find('img').stop().animate({
				'width'     :'199px',
				'height'    :'199px',
				'top'       :'0px',
				'left'      :'-50px',
				'opacity'   :'0.3'
			},500,'easeOutBack');
			$this.find('ul').fadeIn(700);

			$this.find('a:first').addClass('active');
			// Click handler for the entire div, which triggers a "click" of the anchor/link (<a>).
			$this.on('click', function(e) {
				$this.find('a:first')[0].click();
			});
		},
		function () {
			var $this = $(this);
			$this.find('ul').fadeOut(100);
			$this.find('img').stop().animate({
				'width'     :'100px',
				'height'    :'100px',
				'top'       :'0px',
				'left'      :'0px',
				'opacity'   :'1.0'
			},1000,'easeOutBack');
			// Remove the increased z-index value.
			$this.removeClass('z-index-increase');
			$this.find('a:first').removeClass('active');
		}
	);
})(jQuery);
